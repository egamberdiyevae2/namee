import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { BasketContext } from "../App";

const Product = ({ image, title, price, id }) => {
  const { addBasket } = useContext(BasketContext);

  function addNewProduct(e) {
    e.preventDefault();
    addBasket({
      image,
      title,
      id,
      price,
    });
  }

  return (
    <div className="col-md-3 mb-5">
      <div className="card">
        <img src={image} className="card-img-top product-img" alt={title} />
        <div className="card-body">
          <NavLink to={"" + id}>
            <h5 className="card-title">
              {title?.split(" ").slice(0, 2).join("")}
            </h5>
          </NavLink>
          <p className="card-text">{price}$</p>
          <a href="#h" className="btn btn-primary" onClick={addNewProduct}>
            Add Cart
          </a>
        </div>
      </div>
    </div>
  );
};

export default Product;
