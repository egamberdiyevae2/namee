import React, { useContext } from "react";
import { BasketContext } from "../App";
// import Product from "./Product";

const Basket = ({ setBasket }) => {
  const { basket } = useContext(BasketContext);
  const deleteBasket = (id) => setBasket(basket.filter((_) => _.id !== id));
  return (
    <>
      <div>
        {basket.map((item) => {
          return (
            <div
              className="border py-2 mt-3 d-flex align-items-center justify-content-between px-4"
              key={item.title}
            >
              <img src={item.image} alt="img" width={"50px"} className="" />
              <h1 className="text-lowercase">
                {/* {item.title} */}
                {item.title?.split(" ").slice(0, 1).join("")}
              </h1>
              <strong className="p-0 m-0 text-success font-weight-4">
                {item.price}$
              </strong>
              <button
                className="btn btn-danger"
                onClick={deleteBasket(item.id)}
              >
                Remove product
              </button>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default Basket;

// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?
// ?

// import React, { useContext } from "react";
// import { BasketContext } from "../App";

// const Basket = () => {
//   const { basket, setBasket } = useContext(BasketContext);

//   const deleteBasket = (id) => {
//     setBasket((prevBasket) => prevBasket.filter((item) => item.id !== id));
//   };

//   return (
//     <div>
//       {basket.map((item) => (
//         <div
//           className="border py-2 mt-3 d-flex align-items-center justify-content-between px-4"
//           key={item.id}
//         >
//           <h1 className="text-lowercase">{item.title}</h1>
//           <strong className="p-0 m-0 text-success font-weight-4">
//             {item.price}$
//           </strong>
//           <button
//             className="btn btn-danger"
//             onClick={() => deleteBasket(item.id)}
//           >
//             Remove product
//           </button>
//         </div>
//       ))}
//     </div>
//   );
// };

// export default Basket;
