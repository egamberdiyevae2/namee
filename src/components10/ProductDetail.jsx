import React, { useEffect, useState } from "react";
// import Product from "./Product";
import { useParams } from "react-router-dom";
import { useAxios } from "../hooks10/useAxios";

const ProductDetail = () => {
  const [data, setData] = useState({});
  const { res } = useAxios();
  const { id } = useParams();
  useEffect(() => {
    setData(res[id - 1]);
  }, [res]);
  console.log(data);

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-4">
          <img src={data?.image} alt="" className="w-100" />
        </div>
        <div className="col-md-8 text-start">
          <h1> {data?.id + " " + data?.title}</h1>
          <p> {data?.description}</p>
          <p>Rating:🟢 {data?.rating?.rate}</p>
          <p>Left: {data?.rating?.count}</p>
          <h3> {data?.price}$</h3>

          <button className="btn btn-dark btn-lg">back to products</button>

          <button className="btn btn-info btn-lg ms-5">Add to cart</button>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
