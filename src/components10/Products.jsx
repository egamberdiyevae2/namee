import React from "react";
import Product from "./Product";
import { useAxios } from "../hooks10/useAxios";

const Products = () => {
  const { res } = useAxios();

  return (
    <div className="row">
      {res?.map((data) => (
        <Product {...data} key={data.id} />
      ))}
    </div>
  );
};

export default Products;
