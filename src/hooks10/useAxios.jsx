import axios from "axios";
import { useEffect, useState } from "react";

axios.defaults.baseURL = "https://fakestoreapi.com";

export function useAxios() {
  const [res, setRes] = useState([]);
  const [error, setError] = useState("");
  const [loader, setLoader] = useState(true);

  useEffect(() => {
    return async () => {
      try {
        const res = await axios.get("/products");
        setRes(res.data);
        console.log(res.data);
      } catch (err) {
        setError(err);
      } finally {
        setLoader(false);
      }
    };
  }, []);

  return { res, error, loader };
}
