import {
  RouterProvider,
  createBrowserRouter,
  createRoutesFromElements,
  Route,
} from "react-router-dom";
import "./App.css";
import RootLayout from "./layout10/RootLayout";
import Home from "./components10/Home";
import Products from "./components10/Products";
// import { useAxios } from "./hooks10/useAxios";
import ProductLayout from "./layout10/ProductLayout";
import ProductDetail from "./components10/ProductDetail";
import { createContext, useState } from "react";
import Basket from "./components10/Basket";

// export const BasketContext = createContext();

export const BasketContext = createContext();

function App() {
  const [basket, setBasket] = useState([]);
  console.log(basket);

  const addBasket = (param) => setBasket((prev) => [...prev, param]);

  const deleteBasket = (id) => setBasket(basket.filter((_) => _.id !== id));

  const routes = createBrowserRouter(
    createRoutesFromElements(
      <Route path="/" element={<RootLayout />}>
        <Route index element={<Home />} />
        <Route path={"products"} element={<ProductLayout />}>
          <Route index element={<Products />} />
          <Route path={":id"} element={<ProductDetail />} />
        </Route>
        <Route path="carts" element={<Basket setBasket={setBasket} />} />
      </Route>
    )
  );

  return (
    <BasketContext.Provider value={{ basket, addBasket, deleteBasket }}>
      <div className="App">
        <RouterProvider router={routes} />
      </div>
    </BasketContext.Provider>
  );
}

export default App;

// import React, { createContext, useContext, useEffect, useState } from "react";
// import Post from "./components/Post";
// export const CounterContext = createContext();

// const App = () => {
//   const [data, setData] = useState([]);
//   const [count, setCount] = useState(0);
//   useEffect(() => {
//     fetch("http://localhost:3000/posts")
//       .then((res) => res.json())
//       .then((res) => setData(res));
//   }, []);
//   return (
//     <CounterContext.Provider value={{ count, setCount }}>
//       <div className="App">
//         {data.map((post) => (
//           <Post {...post} key={post.id} />
//         ))}
//       </div>
//     </CounterContext.Provider>
//   );
// };

// export default App;
